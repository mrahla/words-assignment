from bs4 import BeautifulSoup
import requests
import html5lib

url = "https://www.ebay.com/usr/shoesparadise.ita"
response = requests.get(url, timeout=5)
content = BeautifulSoup(response.content, "html.parser")
# content = BeautifulSoup(response.content,"html5lib")

bi = content.find("div", attrs={"id": "index_card"})
header = bi.find("span", attrs={"class": "mbg-l"})
name = header.find("a", attrs={"class": "mbg-id"})
rating = header.contents[2].contents[1]
bio = bi.find("h2", attrs={"class": "bio"}).text  # .encode("utf-8")


businessInfo = {
    "name": name.contents[1] if name else "",
    "rating": rating if rating else "",
    "bio": bio,
}

print(businessInfo)

