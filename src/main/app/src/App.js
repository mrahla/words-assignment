import React, { Component } from "react";
import "./App.css";

const API_URL = "http://localhost:7888";

class App extends Component {
  constructor() {
    super();
    this.state = {
      word: "",
      words: [],
      message: ""
    };
  }

  handleChange = event => {
    this.setState({ word: event.target.value });
  };

  handleAddClick = () => {
    if (this.state.word != null && this.state.word != "") {
      fetch(API_URL + "/insert/" + this.state.word, { method: "POST" })
        .then(res => res.json())
        .then(result => {
          if (result.success) {
            this.setState({ message: "Word added" });
            this.setState({ word: "" });
          } else {
            this.setState({ message: "Error" });
          }
        });
    } else {
      this.setState({ message: "Missing word" });
    }
  };

  handleShowClick = () => {
    if (this.state.word != null && this.state.word != "") {
      fetch(API_URL + "/match/" + this.state.word)
        .then(res => res.json())
        .then(result => {
          this.setState({ words: result.top });
        });
      this.setState({ message: "Showing match words for " + this.state.word });
    }
  };

  render() {
    return (
      <div className="App">
        <div>
          <h1>Welcom to my words app</h1>
          <div>
            Insert word:
            <input
              className="word-input"
              value={this.state.word}
              type="text"
              placeholder="word"
              onChange={this.handleChange}
            />
            <div>{this.state.message}</div>
          </div>
          <div>
            <button onClick={this.handleAddClick}>Add word</button>
            <button onClick={this.handleShowClick}>Show matched words</button>
          </div>
          <div className="">
            {this.state.words.map(w => (
              <div>{w}</div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
