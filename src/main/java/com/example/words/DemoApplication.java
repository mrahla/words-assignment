package com.example.words;

import java.util.HashSet;

import com.example.words.services.WordsService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
public class DemoApplication {

	@RequestMapping(value = "/insert/{word}", method = RequestMethod.POST)
	public ResponseStatus Insert(@PathVariable("word") String word) {
		WordsService service = new WordsService();
		Boolean status = service.inserWord(word);
		return new ResponseStatus(status);
	}

	@RequestMapping(value = "/match/{word}", method = RequestMethod.GET)
	public ResponseWords Match(@PathVariable("word") String word) {
		WordsService service = new WordsService();
		HashSet<String> result = service.getMatchWords(word, 3);
		return new ResponseWords(word, result.toArray(new String[0]));
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
