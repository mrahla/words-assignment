package com.example.words;

import java.util.Random;

public class ResponseStatus {
    private final boolean Success;
    private final int Err;

    public ResponseStatus(boolean status) {
        this.Success = status;
        this.Err = new Random().nextInt(100);
    }

    public boolean getSuccess() {
        return Success;
    }

    public int getErr() {
        return Err;
    }
}