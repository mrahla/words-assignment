package com.example.words.services;

import java.util.ArrayList;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class WordsRepository {

    private TreeMap<Integer, String> map = new TreeMap<Integer, String>();

    private static WordsRepository instance = null;

    private WordsRepository() {
    }

    public static WordsRepository getInstance() {
        if (instance == null)
            instance = new WordsRepository();
        return instance;
    }

    public void put(Integer score, String word) {
        map.put(score, word);
    }

    public Map.Entry<Integer, String> getFloorEntry(Integer score) {
        return map.floorEntry(score);
    }

    public Map.Entry<Integer, String> getceilingEntry(Integer score) {
        return map.ceilingEntry(score);
    }

    public Map.Entry<Integer, String> getLower(Integer key) {
        return map.lowerEntry(key);
    }

    public Map.Entry<Integer, String> getHigher(Integer key) {
        return map.higherEntry(key);
    }

    public Integer getSize() {
        return map.size();
    }

    public Integer[] getKeys() {
        return map.keySet().toArray(new Integer[map.size()]);
    }

}
