package com.example.words.services;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class WordsService {

    public WordsService() {
    }

    public Boolean inserWord(String word) {
        if (word != null && !word.isEmpty()) {
            WordsRepository repo = WordsRepository.getInstance();
            repo.put(getAsciiSum(word), word);
            return true;
        }
        return false;
    }

    public HashSet<String> getMatchWords(String word, Integer amount) {
        WordsRepository repo = WordsRepository.getInstance();
        HashSet<String> words = new HashSet<String>();
        Integer ascii = getAsciiSum(word), i = 0, repoSize = repo.getSize();
        Map.Entry<Integer, String> f = repo.getFloorEntry(ascii);
        Map.Entry<Integer, String> c = repo.getceilingEntry(ascii);

        while (words.size() < amount && repoSize > i) {
            if (f != null && c != null) {
                if (Math.abs(ascii - f.getKey()) < Math.abs(ascii - c.getKey())) {
                    words.add(f.getValue());
                    f = repo.getLower(f.getKey());
                } else {
                    words.add(c.getValue());
                    c = repo.getHigher(c.getKey());
                }
            } else if (f != null || c != null) {
                if (f != null) {
                    words.add(f.getValue());
                    f = repo.getLower(f.getKey());
                } else {
                    words.add(c.getValue());
                    c = repo.getHigher(c.getKey());
                }
            }
            i++;
        }

        return words;
    }

    private int getAsciiSum(String word) {
        int asciiSum = 0;
        for (int i = 0; i < word.length(); i++) {
            asciiSum += (int) word.charAt(i);
        }
        return asciiSum;
    }

}